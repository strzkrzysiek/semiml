
exception Fatal_error
exception Not_implemented of string

let error_counter = ref 0

let error_p pos msg =
  error_counter := !error_counter + 1;
  Printf.eprintf ("%s: error: " ^^ msg ^^ "\n")
    (Common.Position.to_string pos)

let warning_p pos msg =
  Printf.eprintf ("%s: warning: " ^^ msg ^^ "\n")
    (Common.Position.to_string pos)

let note_p pos msg =
  Printf.eprintf ("%s: note: " ^^ msg ^^ "\n")
    (Common.Position.to_string pos)

let error_lp pos msg =
  error_p (Common.Position.of_lexing pos) msg

let warning_lp pos msg =
  warning_p (Common.Position.of_lexing pos) msg

let note_lp pos msg =
  note_p (Common.Position.of_lexing pos) msg

let error_pp p1 p2 msg =
  error_p (Common.Position.of_pp p1 p2) msg

let warning_pp p1 p2 msg =
  warning_p (Common.Position.of_pp p1 p2) msg

let note_pp p1 p2 msg =
  note_p (Common.Position.of_pp p1 p2) msg

let error_no_pos msg =
  error_counter := !error_counter + 1;
  Printf.eprintf ("error: " ^^ msg ^^ "\n")

let warning_no_pos msg =
  Printf.eprintf ("warning: " ^^ msg ^^ "\n")

let note_no_pos msg =
  Printf.eprintf ("note: " ^^ msg ^^ "\n")

let error ?tag msg =
  match tag with
  | None -> error_no_pos msg
  | Some tag ->
    begin match MetaData.Position.try_get tag with
    | None     -> error_no_pos msg
    | Some pos -> error_p pos msg
    end

let warning ?tag msg =
  match tag with
  | None -> warning_no_pos msg
  | Some tag ->
    begin match MetaData.Position.try_get tag with
    | None     -> warning_no_pos msg
    | Some pos -> warning_p pos msg
    end

let note ?tag msg =
  match tag with
  | None -> note_no_pos msg
  | Some tag ->
    begin match MetaData.Position.try_get tag with
    | None     -> note_no_pos msg
    | Some pos -> note_p pos msg
    end

let internal_error msg =
  Printf.eprintf ("INTERNAL ERROR: " ^^ msg ^^ "\n")

let not_implemented name =
  error "Not implemented: %s" name;
  raise (Not_implemented name)
