type t =
| MetaCmd_Eval
| MetaCmd_Pretty
| MetaCmd_Require of string list

let run state mc =
  let node = CompilerGraph.node_of_state state in
  match mc with
  | MetaCmd_Eval   ->
    let path = CompilerGraph.find_path_to_eval node in
    CompilerGraph.run_path path state
  | MetaCmd_Pretty ->
    let path = CompilerGraph.find_path_to_pretty node in
    CompilerGraph.run_path path state
  | MetaCmd_Require contracts ->
    let path = CompilerGraph.find_path_to_contract_names node contracts in
    CompilerGraph.run_path path state
