
open Lang.NuL.Ast

let fv x expr =
  Common.Var.Set.mem x (MetaData.NuL.FreeVars.get expr.e_tag)

let rec transform expr =
  match expr.e_kind with
  | (Succ | Var _ | Num _) -> expr
  | Abs(x, body) ->
    { expr with e_kind = Abs(x, transform body) }
  | App(e1, e2) ->
    let e1 = transform e1 in
    begin match e1.e_kind with
    | Abs(x, body) when not (fv x body) -> body
    | _ -> { expr with e_kind = App(e1, transform e2) }
    end
  | Case(e1, (x2, e2), e3, (x4, e4)) ->
    { expr with e_kind =
      Case(transform e1, (x2, transform e2), transform e3, (x4, transform e4))
    }

let c_unused_let = Contract.create "transform:unused_let"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_NuL
    ~target: Compiler.Lang_NuL
    ~name:   "NuL:unused_let"
    ~require:
      [ Lang.NuL.Contracts.unique_tags
      ; Analysis.NuL.FreeVars.contract
      ]
    ~contracts:
      [ Lang.NuL.Contracts.unique_tags
      ; c_unused_let
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.NuL.Contracts.right_scopes
      ; Contract.saves_contract Lang.NuL.Contracts.unique_vars
      ]
    transform
