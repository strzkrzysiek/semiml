
open ContractChecker

let checkers_by_contract = Hashtbl.create 32

let register_contract_checker checker =
  let (ContractChecker(_, data)) = checker in
  Hashtbl.add 
    checkers_by_contract 
    data.contract 
    checker

let possible_checkers lang contract =
  List.filter (fun (ContractChecker(c_lang, _)) ->
    Types.Language.equal lang c_lang
  ) (Hashtbl.find_all checkers_by_contract contract)
