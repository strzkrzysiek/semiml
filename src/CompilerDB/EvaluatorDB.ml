
open Evaluator

let evaluator_list = ref []

let register_evaluator eval_obj =
  evaluator_list := eval_obj :: !evaluator_list

let possible_evaluators lang contracts =
  List.filter (fun eval_obj ->
    match eval_obj with
    | Evaluator(lang2, data) ->
      Types.Language.equal lang lang2 &&
      List.for_all (fun c -> Contract.Set.mem c contracts) data.require
  ) !evaluator_list
