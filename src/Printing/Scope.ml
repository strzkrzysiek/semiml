
module type S = sig
  type key
  type t

  val create : 
    ?attributes: Box.attribute list -> 
      (key -> string) -> t

  val pretty : t -> key -> Box.t
  val extend : t -> key -> t * Box.t
end

module type OrderedType = sig
  type t
  val compare : t -> t -> int
end

module Make(Key : OrderedType) 
  : S with type key = Key.t =
struct
  type key = Key.t

  module KeyMap = Map.Make(Key)
  module StrSet = Set.Make(String)

  type t =
    { name_of    : key -> string
    ; attributes : Box.attribute list
    ; key_map    : string KeyMap.t
    ; used_names : StrSet.t
    }

  let fresh_name used_names base_name =
    if StrSet.mem base_name used_names then
      let rec aux n =
        let name = base_name ^ string_of_int n in
        if StrSet.mem name used_names then aux (n+1)
        else name
      in aux 0
    else base_name

  let create ?(attributes=[Box.Variable]) name_of =
    { name_of    = name_of
    ; attributes = attributes
    ; key_map    = KeyMap.empty
    ; used_names = StrSet.empty
    }

  let pretty scope key =
    try
      let name = KeyMap.find key scope.key_map in
      Box.text 
        ~attributes: scope.attributes
        name
    with
    | Not_found ->
      Box.error (scope.name_of key)

  let extend scope key =
    let name = fresh_name scope.used_names (scope.name_of key) in
    let scope =
      { scope with
        key_map    = KeyMap.add key name scope.key_map
      ; used_names = StrSet.add name scope.used_names
      } in
    let box = Box.text ~attributes: scope.attributes name in
    (scope, box)
end
