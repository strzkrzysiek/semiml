
open DataTypes

let rec ugly box =
  match box with
  | B_Text(_, str) -> str
  | B_Prefix(box1, box2) -> ugly box1 ^ ugly box2
  | B_Suffix(box1, box2) -> ugly box1 ^ ugly box2
  | B_Indent(_, box) -> ugly box
  | B_WhiteSep box -> " " ^ ugly box
  | B_BreakLine box -> ugly box ^ "\n"
  | B_Box boxes ->
    List.fold_left (fun str box -> str ^ ugly box) "" boxes

let print_stdout box =
  print_endline (ugly box)
