
type (_, _) t =
| Eq  : ('a, 'a) t
| Neq : ('a, 'b) t
