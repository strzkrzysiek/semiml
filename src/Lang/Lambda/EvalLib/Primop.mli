
val eval_prim_arith   : Common.Primop.Arith.t   -> Types.value
val eval_prim_farith  : Common.Primop.FArith.t  -> Types.value
val eval_prim_mem     : Common.Primop.Mem.t     -> Types.value
val eval_prim_repr    : Common.Primop.Repr.t    -> Types.value
val eval_prim_control : Common.Primop.Control.t -> Types.value
