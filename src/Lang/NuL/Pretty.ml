
open Printing

module Env = Printing.Scope.Make(Common.Var)

let rec pretty_expr env prec expr =
  match expr.Ast.e_kind with
  | Ast.Succ  -> Box.keyword "succ"
  | Ast.Num n -> Box.int n
  | Ast.Var x -> Env.pretty env x
  | Ast.Abs _ -> Box.prec_paren 0 prec (
      Box.box
      (  Box.keyword "fun"
      :: pretty_abs env expr)
    )
  | Ast.App(e1, e2) -> Box.prec_paren 1 prec (
      Box.box
      [ pretty_expr env 1 e1
      ; Box.indent 2 (Box.white_sep (pretty_expr env 2 e2))
      ]
    )
  | Ast.Case(expr, case_fun, case_zero, case_succ) ->
    Box.prec_paren 0 prec (
      Box.box
      [ Box.box
        [ Box.keyword "case"
        ; Box.indent 2 (Box.white_sep (pretty_expr env 0 expr))
        ; Box.white_sep (Box.keyword "of")
        ] |> Box.br
      ; pretty_case_fun  env case_fun  |> Box.br
      ; pretty_case_zero env case_zero |> Box.br
      ; pretty_case_succ env case_succ
      ]
    )

and pretty_case_fun env (x, body) =
  let (env, x) = Env.extend env x in
  Box.box
  [ Box.box
    [ Box.oper "|"
    ; Box.white_sep (Box.keyword "fun")
    ; Box.white_sep x
    ; Box.white_sep (Box.oper "->")
    ]
  ; Box.indent 2 (Box.white_sep (pretty_expr env 0 body))
  ]

and pretty_case_zero env body =
  Box.box
  [ Box.box
    [ Box.oper "|"
    ; Box.white_sep (Box.keyword "zero")
    ; Box.white_sep (Box.oper "->")
    ]
  ; Box.indent 2 (Box.white_sep (pretty_expr env 0 body))
  ]

and pretty_case_succ env (x, body) =
  let (env, x) = Env.extend env x in
  Box.box
  [ Box.box
    [ Box.oper "|"
    ; Box.white_sep (Box.keyword "succ")
    ; Box.white_sep x
    ; Box.white_sep (Box.oper "->")
    ]
  ; Box.indent 2 (Box.white_sep (pretty_expr env 0 body))
  ]

and pretty_abs env expr =
  match expr.Ast.e_kind with
  | Ast.Abs(x, body) ->
    let (env, x) = Env.extend env x in
    Box.indent 2 (Box.white_sep x) :: pretty_abs env body
  | _ ->
    [ Box.white_sep (Box.oper "->")
    ; Box.indent 2 (Box.white_sep (pretty_expr env 0 expr))
    ]

let pretty_program expr =
  pretty_expr (Env.create Common.Var.name) 0 expr
