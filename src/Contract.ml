
module Contr = struct
  type t =
    { name : string
    ; id   : int64
    }

  let compare c1 c2 = Int64.compare c1.id c2.id
end

include Contr

let equal c1 c2 = c1 == c2

let next_fresh_id = ref 0L
let fresh_id () =
  let r = !next_fresh_id in
  next_fresh_id := Int64.succ r;
  r

let create name =
  { id   = fresh_id ()
  ; name = name
  }

let name c = c.name

type action =
| Add    of t list
| Set    of t list
| Remove of t list

type rule = t list * action

let saves_contract c = ([c], Add [c])

module Set = Set.Make(Contr)

let apply_rule contracts (cnd, action) new_contracts =
  if List.for_all (fun c -> Set.mem c contracts) cnd then
    match action with
    | Add cs    -> Set.union new_contracts (Set.of_list cs)
    | Set cs    -> Set.of_list cs
    | Remove cs -> Set.diff new_contracts (Set.of_list cs)
  else new_contracts

let apply_rules rules contracts =
  List.fold_left (fun new_contracts rule ->
    apply_rule contracts rule new_contracts
  ) contracts rules
